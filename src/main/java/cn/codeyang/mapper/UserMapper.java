package cn.codeyang.mapper;

import cn.codeyang.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/2/17
 */
@Repository
public interface UserMapper {
    List<User> selectAll();
}
